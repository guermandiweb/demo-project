<?php
/* register CPT */
add_action('init', 'create_selected_work');

function create_selected_work() {
  register_post_type('selected_work', array(
    'public' => true,
    'show_in_rest' => true,
    'has_archive' => true,
    'labels' => array(
      'name' => 'Selected Works',
      'add_new_item' => 'Add Work',
      'edit_item' => 'Edit Work',
      'all_items' => 'All Works',
      'singular_name' => 'Work'
    ),
    'menu_icon' => 'dashicons-portfolio'
  ));
};

add_action('init', 'create_list');

function create_list() {
  register_post_type('list', array(
    'public' => true,
    'show_in_rest' => true,
    'has_archive' => true,
    'labels' => array(
      'name' => 'Lists',
      'add_new_item' => 'Add List',
      'edit_item' => 'Edit List',
      'all_items' => 'All Lists',
      'singular_name' => 'List'
    ),
    'menu_icon' => 'dashicons-networking'
  ));
};