export default {
    init() {

    },
    finalize() {
        // paragraph shows on title:hover
        const workflow = document.querySelectorAll('.workflow-box-description')
        workflow.forEach(box => {
            box.addEventListener('mouseenter', function() {
                this.children[1].style.opacity = 1
            })

        })
    },
};