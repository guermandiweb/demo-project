import { gsap } from 'gsap'

export default {
  init() {
    
  },
  finalize() { 
    //----------------//
    // HERO ANIMATION //
    //----------------//
    const marketing = document.querySelector('#title-marketing'),
    design = document.querySelector('#title-design'),
    comunicazione = document.querySelector('#title-comunicazione'),
    audiovideo = document.querySelector('#title-audiovideo'),
    descr = document.querySelector('#cosa-facciamo')
    var tl = gsap.timeline()
    if (!sessionStorage.getItem('ourWorkIntroRunOnce')) {
    tl.from(marketing, {
      autoAlpha: 0,
      x: '-400%',
      duration: 0.7,
      ease: 'power1.out',
    })
    .from(design, {
      autoAlpha: 0,
      y: '-400%',
      duration: 0.7,
      ease: 'power1.out',
    }, 0.3)
    .from(comunicazione, {
      autoAlpha: 0,
      x: '400%',
      duration: 0.7,
      ease: 'power1.out',
    },0.6)
    .from(audiovideo, {
      autoAlpha: 0,
      y: '800%',
      duration: 0.7,
      ease: 'power1.out',
    },0.9)
    .from(descr, {
      autoAlpha: 0,
      x: '400%',
      duration: 1,
      ease: 'power4.out',
    }, 1.2)
    sessionStorage.setItem('ourWorkIntroRunOnce', true);
  } else {
    marketing.style.visibility = 'visible'
    design.style.visibility = 'visible'
    comunicazione.style.visibility = 'visible'
    audiovideo.style.visibility = 'visible'
  }
  },
}