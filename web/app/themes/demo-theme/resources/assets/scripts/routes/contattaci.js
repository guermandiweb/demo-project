import { gsap } from 'gsap'
import { CustomEase } from 'gsap/CustomEase';
gsap.registerPlugin(CustomEase)
export default {
  init() {
    // JavaScript to be fired on the contattaci page

  },
  finalize() {
    let
    // letters = document.querySelector('.letters'),
    // letters2 = document.querySelector('.letters2'),
    // letters3 = document.querySelector('.letters3'),

    piuma = document.querySelector('.piuma-img'),
    btn1 = document.querySelector('.btn1'),
    btn2 = document.querySelector('.btn2')
    /////////////////////
    // INTRO ANIMATION //
    /////////////////////
    if (!sessionStorage.getItem('contactIntroRunOnce')) {
      CustomEase.create('piuma', 'M0,0 C0,0 0,0.069 0.022,0.172 0.027,0.197 0.044,0.247 0.06,0.266 0.08,0.29 0.103,0.298 0.156,0.31 0.24,0.328 0.214,0.308 0.372,0.358 0.467,0.388 0.502,0.4 0.56,0.494 0.581,0.528 0.568,0.561 0.664,0.746 0.698,0.812 0.674,0.822 0.788,0.932 0.856,0.997 1,1 1,1 ')
      var tl = gsap.timeline()
      let
      line1 = document.querySelector('.check-line-1'),
      line2 = document.querySelector('.check-line-2'),
      line3 = document.querySelector('.check-line-3')
      // letters.innerHTML = letters.textContent.replace(/\S/g, '<span class="letter">$&</span>')
      // letters2.innerHTML = letters2.textContent.replace(/\S/g, '<span class="letter2">$&</span>')
      // letters3.innerHTML = letters3.textContent.replace(/\S/g, '<span class="letter3">$&</span>')
        tl.from(line1, {
          autoAlpha: '0',
          // y: '40px',
          duration: 0.5,
          // stagger: 0.1,
          ease: 'power4.out',
        })
        .from(line2, {
          autoAlpha: '0',
          // y: '40px',
          duration: 0.5,
          // stagger: 0.1,
          ease: 'power4.out',
        })
        .from(line3, {
          autoAlpha: '0',
          // y: '40px',
          duration: 0.5,
          // stagger: 0.1,
          ease: 'power4.out',
        })
        .from(piuma, {
          autoAlpha: 0,
          y: '-400%',
          rotate: '540deg',
          duration: 5,
          ease: 'piuma',
        }, -2)
        .from(btn1, {
          autoAlpha: 0,
        })
        .from(btn2, {
          autoAlpha: 0,
        })
      sessionStorage.setItem('contactIntroRunOnce', true);
  }

  // Audio

//   let audioBtn = document.querySelector('.audio-text'),
//   video = document.querySelector('#checkVid')
//   audioBtn.addEventListener('click', function(){
//     if( video.muted ) {
//       video.volume = 1
//       video.muted = !video.muted
//       this.textContent = 'mute'
//     } else {
//       video.volume = 0
//       this.textContent = 'unmute'
//       video.muted = true
//     }
// })

    //----------------//
    // Showreel Modal //
    //----------------//
    let cta = document.querySelectorAll('[data-video]'),
    modal = document.querySelector('#valutazione')

    cta.forEach(c => {
      c.addEventListener('click', function(e) {
        console.log('click');
        e.preventDefault()
        modal.classList.add('show-video')
        let close = modal.firstElementChild
        setTimeout(() => {
          close.classList.remove('close-video')
        }, 1000);
        close.addEventListener('click', function() {
          let video = modal.querySelector('video')
          close.classList.add('close-video')
          video.pause()
          video.currentTime = '0'
          setTimeout(() => {
            modal.classList.remove('show-video')
          }, 250);
        })
      })
    })
  },
};
