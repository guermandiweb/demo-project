import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)


export default {
    init() {},
    finalize() {
        ///////////////////////
        // title size adjust //
        ///////////////////////
        let selectedWorkTitle = document.querySelector('.caso-col-title'),
            titleLength = selectedWorkTitle.textContent.length,
            selectedWorkMotto = document.querySelector('.caso-col-title-shortdescr')
        if (titleLength > 15 && titleLength < 18) {
            selectedWorkTitle.classList.add('three-quarter')
            selectedWorkMotto.classList.add('three-eight')
        } else if (titleLength >= 18) {
            selectedWorkTitle.classList.add('half')
            selectedWorkMotto.classList.add('quarter')
        }
        ///////////////////////////
        // HERO scroll animation //
        ///////////////////////////
        let casoTitle = document.querySelector('#caso-title'),
            casoHeroImg = document.querySelector('.caso-hero-image')
        if (window.innerWidth > 768) {
            // console.log(window.innerWidth)
            gsap.to(casoTitle, {
                scrollTrigger: {
                    trigger: casoHeroImg,
                    start: 'center center',
                    end: 'top top',
                    toggleActions: 'play none reverse none',
                    markers: false,
                },
                opacity: 0,
                duration: 0.2,
            })
        }

        ///////////
        // Video //
        ///////////
        // let video = document.querySelector('video')
        // console.log(video.autoplay)
        // if (video) {
        //     gsap.from(video, {
        //         scrollTrigger: {
        //             trigger: video,
        //             start: 'top bottom',
        //             end: 'top top',
        //             toggleActions: 'play none none reset',
        //             markers: false,
        //             scrub: false,
        //             once: true,
        //         },
        //         opacity: '0',
        //         duration: 1.6,
        //         onStart: () => video.autoplay = true,
        //     })
        // }
        //////////////////////////////////////
        // VIDEO FULLSCREEN ON TAP BEHAVIOR //
        //////////////////////////////////////
        // let isFullscreen = false
        // if (video) {
        //     video.addEventListener('click', function() {
        //         if (window.innerWidth < 769) {
        //             if (isFullscreen) {
        //                 video.exitFullscreen()
        //             } else {
        //                 if (video.requestFullscreen) {
        //                     video.requestFullscreen();
        //                 } else if (video.mozRequestFullScreen) {
        //                     video.mozRequestFullScreen();
        //                 } else if (video.webkitRequestFullscreen) {
        //                     video.webkitRequestFullscreen();
        //                 } else if (video.msRequestFullscreen) {
        //                     video.msRequestFullscreen();
        //                 }
        //             }
        //             isFullscreen != isFullscreen
        //         }
        //     })
        // }
        ///////////////////////
        // section animation //
        ///////////////////////
        let sectionDescr = document.querySelector('.details-wrapper'),
            sectionGeneral = document.querySelector('#cosa'),
            sectionGeneralHeight = sectionGeneral.getBoundingClientRect().height,
            sectionCta = document.querySelector('.cta')
        gsap.to(sectionDescr, {
            scrollTrigger: {
                trigger: sectionDescr,
                start: 'top 75%',
                end: 'top center',
                toggleActions: 'play none reverse none',
                markers: false,
                scrub: true,
            },
            y: -sectionGeneralHeight,
            duration: 2.4,
        })
        gsap.to(sectionCta, {
                scrollTrigger: {
                    trigger: sectionCta,
                    start: 'top bottom',
                    end: 'top center',
                    toggleActions: 'play none reverse none',
                    markers: false,
                    scrub: 1,
                },
                y: sectionGeneralHeight,
                duration: 4.8,
            })
            // let sectionGeneral = document.querySelector('#cosa')
            // gsap.to(sectionGeneral, {
            //         scrollTrigger: {
            //             trigger: sectionGeneral,
            //             start: 'top top',
            //             end: 'top 75%',
            //             toggleActions: 'play none reverse none',
            //             markers: false,
            //             scrub: true,
            //         },
            //         y: 500,
            //         duration: 3,
            //     })
            ////////////
            // Slider //
            ////////////
        const sliders = document.querySelectorAll('.slider-container')
        sliders.forEach(slider => {
            let slides = Array.from(slider.querySelectorAll('.slide'))
                // scrollRight = document.querySelector('.scroll-right')
                // set up our state
            let isDragging = false,
                startPos = 0,
                currentTranslate = 0,
                prevTranslate = 0,
                animationID,
                currentIndex = 0

            // add our event listeners
            slides.forEach((slide, index) => {
                const slideImage = slide.querySelector('img')
                    // test hover
                slideImage.addEventListener('mouseenter', function() {
                    slideImage.style.transform = 'scale(0.95)'
                        // setTimeout(() => {
                        //   slideImage.style.transform = 'scale(1)'
                        // }, 300);
                })
                slideImage.addEventListener('mouseout', function() {
                        slideImage.style.transform = 'scale(1)'
                    })
                    // disable default image drag
                slideImage.addEventListener('dragstart', (e) => e.preventDefault())
                    // touch events
                slide.addEventListener('touchstart', touchStart(index))
                slide.addEventListener('touchend', touchEnd)
                slide.addEventListener('touchmove', touchMove)
                    // mouse events
                slide.addEventListener('mousedown', touchStart(index))
                slide.addEventListener('mouseup', touchEnd)
                slide.addEventListener('mousemove', touchMove)
                slide.addEventListener('mouseleave', touchEnd)
            })

            // make responsive to viewport changes
            window.addEventListener('resize', setPositionByIndex)

            // prevent menu popup on long press
            window.oncontextmenu = function(event) {
                event.preventDefault()
                event.stopPropagation()
                return false
            }

            function getPositionX(event) {
                return event.type.includes('mouse') ? event.pageX : event.touches[0].clientX
            }

            // use a HOF so we have index in a closure
            function touchStart(index) {
                return function(event) {
                    currentIndex = index
                    startPos = getPositionX(event)
                    isDragging = true
                    animationID = requestAnimationFrame(animation)
                    slider.classList.add('grabbing')
                }
            }

            function touchMove(event) {
                if (isDragging) {
                    const currentPosition = getPositionX(event)
                    currentTranslate = prevTranslate + currentPosition - startPos
                        // scrollRight.style.opacity = 0
                        // setTimeout(() => {
                        //   scrollRight.classList.add('d-none')
                        // }, 500);
                }
            }

            function touchEnd() {
                cancelAnimationFrame(animationID)
                isDragging = false
                const movedBy = currentTranslate - prevTranslate
                    // if moved enough negative then snap to next slide if there is one
                if (movedBy < -100 && currentIndex < slides.length - 1) {
                    arrowLeft.forEach(arrow => {
                        arrow.style.fill = 'grey'
                    })
                    currentIndex += 1
                }

                // if moved enough positive then snap to previous slide if there is one
                if (movedBy > 100 && currentIndex > 0) {
                    currentIndex -= 1
                } else if (currentIndex === 0) {
                    arrowLeft.forEach(arrow => {
                        arrow.style.fill = '#f0f5f700'
                    })
                }

                setPositionByIndex()

                slider.classList.remove('grabbing')
            }

            function animation() {
                setSliderPosition()
                if (isDragging) requestAnimationFrame(animation)
            }

            function setPositionByIndex() {
                currentTranslate = currentIndex * (-window.innerWidth * 0.75)
                prevTranslate = currentTranslate
                setSliderPosition()
            }

            function setSliderPosition() {
                slider.style.transform = `translateX(${currentTranslate}px)`
            }

            // arrow behavior
            let clickOnSlide = document.querySelector('.scroll-left'),
                arrowLeft = document.querySelectorAll('.arrow rect')

            clickOnSlide.addEventListener('click', function() {
                    if (currentIndex > 0) {
                        currentTranslate = currentIndex * (-window.innerWidth * 0.75)
                            // console.log(currentTranslate)
                        slider.style.transform = `translateX(${currentTranslate + window.innerWidth * 0.75}px)`
                        currentIndex -= 1
                            // console.log(currentIndex);
                    } else {
                        arrowLeft.forEach(arrow => {
                            arrow.style.fill = '#f0f5f700'
                        })
                    }
                })
                // slider slides left on scroll

            if (!isDragging) {
                gsap.from(slider, {
                    scrollTrigger: {
                        trigger: slider,
                        start: 'top bottom',
                        end: 'top top',
                        toggleActions: 'play none reverse none',
                        markers: false,
                        scrub: false,
                        once: true,
                    },
                    x: '160',
                    duration: 1.6,
                })
            } else {
                gsap.from(slider, {
                    x: '0',
                    duration: 0.2,
                    once: true,
                })
            }
        })

    },
}