import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export default {
    init() {
        //------------//
        // FOUC TRICK //
        //------------//
        let domReady = (cb) => {
            document.readyState === 'interactive' || document.readyState === 'complete' ?
                cb() :
                document.addEventListener('DOMContentLoaded', cb);
        };

        domReady(() => {
            // Display body when DOM is loaded
            document.body.style.visibility = 'visible';
        });

        //------------------------//
        // Servizi Title BI-COLOR //
        //------------------------//
        // let serviziTitle = document.querySelector('.servizi-headline-title')
        // if (checkTitle) {
        // let serviziWords = serviziTitle.innerHTML

        // const serviziTitleSplit = serviziWords.split(' ')
        // serviziTitleSplit.splice(1, 1, `<span class='blue'>${serviziTitleSplit[1]}</span>`)
        // if (serviziTitle.innerHTML) {
        //   serviziTitle.innerHTML = serviziTitleSplit.join(' ')
        // }
        // }
        //-------------------------------//
        // Check Gratuito Title BI-COLOR //
        //-------------------------------//
        // let checkTitle = document.querySelector('.check-headline-title')
        // if (checkTitle) {

        //   let checkWords = checkTitle.innerHTML

        //   let checkTitleSplit = checkWords.split(' ')
        //   checkTitleSplit.splice(2, 1, `<span class='pink'>${checkTitleSplit[2]}</span>`)
        //   if (checkTitle.innerHTML) {
        //     checkTitle.innerHTML = checkTitleSplit.join(' ')
        //   }
        // }
    },
    finalize() {
        // header test
        const header = document.querySelector('header')
            // console.log(header)
        window.addEventListener('scroll', function() {
            gsap.to(header, {
                y: '-50%',
                opacity: 0,
                duration: 0.5,
                ease: 'circ.out',
            })
        })

        function createScrollStopListener(element, callback, timeout) {
            var handle = null;
            var onScroll = function() {
                if (handle) {
                    clearTimeout(handle);
                }
                handle = setTimeout(callback, timeout || 200); // default 200 ms
            };
            element.addEventListener('scroll', onScroll);
            return function() {
                element.removeEventListener('scroll', onScroll);
            };
        }

        // Example usage:

        createScrollStopListener(window, function() {
            gsap.to(header, {
                y: 0,
                duration: 0.5,
                opacity: 1,
                ease: 'circ.out',
            })
        });


        //---------------//
        // MENU BEHAVIOR //
        //---------------//
        // MENU SLIDER
        // Burger Animation
        const main = document.querySelector('.wrap'),
            burger = document.querySelector('#burger'),
            menu = document.querySelector('.nav-menu')
        burger.addEventListener('click', function() {
                burger.classList.toggle('is-active')
                openMenu()
            })
            // Menu slides
        const openMenu = () => {
                menu.classList.toggle('show-menu')
            }
            // Menu item click handler
        const menuItem = document.querySelectorAll('.nav-menu-list li a')
        menuItem.forEach(item => {
            item.addEventListener('click', (e) => {
                e.preventDefault()
                burger ? burger.classList.remove('is-active') : ''
                menu.classList.remove('show-menu')
                main.style.opacity = 0
                setTimeout(() => {
                    window.location.href = item.href
                }, 300);
            })
        })

        //------------------------------//
        // Services Navigation Behavior //
        //------------------------------//

        // let text
        //   let text = document.querySelectorAll('.svg-text')
        //   let description = document.querySelectorAll('.servizi-col')

        // text.forEach(item => {
        //   gsap.from(item, {
        //     scrollTrigger: {
        //       trigger: item,
        //       start: 'center 85%',
        //       toggleActions: 'play none reverse none',
        //       markers: false,
        //       scrub: true,
        //     },
        //     opacity: 0,
        //     ease: 'power1.out',
        //   })
        // })
        // text.forEach(item => {
        //   gsap.to(item, {
        //     scrollTrigger: {
        //       trigger: item,
        //       start: 'top 25%',
        //       toggleActions: 'play none reverse none',
        //       scrub: true,
        //       markers: false,
        //     },
        //     x: '-150%',
        //     ease: 'power1.out',
        //   })
        // })
        // description.forEach(descr => {
        //   gsap.from(descr, {
        //     scrollTrigger: {
        //       trigger: descr,
        //       start: 'top center',
        //       end: 'top top',
        //       toggleActions: 'play none reverse none',
        //       markers: false,
        //     },
        //     opacity: 0,
        //     duration: 0.4,
        //     ease: 'power1.out',
        //   })
        // })

    },
}