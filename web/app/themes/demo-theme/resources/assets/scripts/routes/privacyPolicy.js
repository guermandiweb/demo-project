export default {
    init() {},
    finalize() {
        // test

        let btn = document.querySelectorAll('.privacy-btn')
        btn.forEach(b => {
            b.addEventListener('click', rotateArrow)
        })

        function rotateArrow() {
            let arrows = document.querySelectorAll('.arrow-down')
                // all arrows down
            arrows.forEach(a => {
                    a.classList.remove('up')
                })
                // if is active, arrow up
            let activeArrow = this.querySelector('.arrow-down')
            if (this.dataset.target === activeArrow.dataset.target) {
                this.ariaExpanded ? activeArrow.classList.add('up') : activeArrow.classList.remove('up')
            }
        }
    },
}