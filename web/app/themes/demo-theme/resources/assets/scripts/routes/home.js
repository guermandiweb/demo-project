import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

export default {
    init() {},
    finalize() {
        //////////////////////
        // HERO Photos Swap //
        //////////////////////
        // let index = 0,
        //     subjects = Array.from(document.querySelectorAll('.heronew-subject-img')),
        //     count = subjects.length,
        //     hide = 'hide'
        // nextImg()

        // function nextImg() {
        //     subjects[(index++) % count].classList.add(hide)
        //     subjects[index % count].classList.remove(hide)
        //     setTimeout(nextImg, 3000)
        // }
        ///////////////////////
        // KEYWORDS ANMATION //
        ///////////////////////
        gsap.to('.marquee-track.l h1', {
            scrollTrigger: {
                trigger: '.marquee-track.l h1',
                start: 'top bottom',
                end: '400% top',
                scrub: 0.6,
            },
            xPercent: 25,
            duration: 3,
            ease: 'linear',
        })
        gsap.to('.marquee-track.r h1', {
                scrollTrigger: {
                    trigger: '.marquee-track.r h1',
                    end: '400% top',
                    scrub: 0.6,
                },
                xPercent: -25,
                duration: 3,
                ease: 'linear',
            })
            //////////////////////////////////////
            // VIDEO FULLSCREEN ON TAP BEHAVIOR //
            //////////////////////////////////////
            // let video = document.querySelector('video'),
            //     isFullscreen = false
            // video.addEventListener('click', function() {
            //         if (window.innerWidth < 769) {
            //             if (isFullscreen) {
            //                 video.exitFullscreen()
            //             } else {
            //                 if (video.requestFullscreen) {
            //                     video.requestFullscreen();
            //                 } else if (video.mozRequestFullScreen) {
            //                     video.mozRequestFullScreen();
            //                 } else if (video.webkitRequestFullscreen) {
            //                     video.webkitRequestFullscreen();
            //                 } else if (video.msRequestFullscreen) {
            //                     video.msRequestFullscreen();
            //                 }
            //             }
            //             isFullscreen != isFullscreen
            //         }
            //     })
            ////////////////////////////////
            // VIDEO EMBED MODAL BEHAVIOR //
            ////////////////////////////////
            // Gets the video src
            // var $videoSrc = 'https://player.vimeo.com/video/690843660'

        // $('.video-btn').click(function() {
        //         console.log('click')
        //         console.log($videoSrc)
        //         $videoSrc = $(this).data('src')
        //         console.log($videoSrc)
        //     })
        //     // when the modal is opened autoplay it  
        // $('#myModal').on('shown.bs.modal', function() {
        //         if ($('.closevideo').hasClass('close-video')) {
        //             $('.closevideo').removeClass('close-video')
        //         }
        //         // set the video src to autoplay and not to show related video
        //         $('#video').attr('src', $videoSrc + '?autoplay=1&ampmodestbranding=1&ampshowinfo=0')
        //     })
        //     // stop playing the video when I close the modal
        // $('#myModal').on('hide.bs.modal', function() {
        //     $('.closevideo').addClass('close-video')
        //         // a poor man's stop video
        //     $('#video').attr('src', '')
        // })
    },
}