// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import chiSiamo from './routes/chiSiamo';
import cosaFacciamo from './routes/cosaFacciamo';
import casiStudio from './routes/casiStudio';
import singleSelectedWork from './routes/singleSelectedWork';
import contattaci from './routes/contattaci';
import privacyPolicy from './routes/privacyPolicy';

const routes = new Router({
    common,
    home,
    chiSiamo,
    cosaFacciamo,
    casiStudio,
    singleSelectedWork,
    contattaci,
    privacyPolicy,
});

// Load Events
$(window).on('load', () => routes.loadEvents());