@extends('layouts.app')
@section('content')
  {{-- @while(have_posts()) @php the_post() @endphp --}}
    @include('partials.selected-work-hero')
@include('partials.selected-work-hero-image')
@include('partials.selected-work-overview')
@include('partials.selected-work-video')
@include('partials.selected-work-cosa')
<div class="details-wrapper">
  @include('partials.selected-work-descr')
  @include('partials.selected-work-risultato')
  @include('partials.selected-work-testimonial')
  @include('partials.cta')
</div>
  {{-- @endwhile --}}
@endsection
