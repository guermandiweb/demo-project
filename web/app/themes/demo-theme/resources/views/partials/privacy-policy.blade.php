<section class="privacy container">
  <div class="privacy-row privacy-row-top row">
    <h1 class="privacy-title">Privacy Policy & Informativa Cookie</h1>
  </div>
  <div id="accordion" class="privacy-accordion">
    <div class="privacy-row privacy-row-bottom row">
      <div class="privacy-col col-12 col-md-6">
        <!-- ---------- -->
        <!-- TITLE LEFT -->
        <!-- ---------- -->
        <h2 class="privacy-subtitle">Informativa trattamento dati personali</h2>
        <p class="privacy-descr">Mai e per nessuna ragione vendiamo o affittiamo i Dati degli Utenti a terze parti. I
          soli
          usi dei Dati sono quelli evidenziati in questa policy. Gli Utenti sono gli unici proprietari dei loro Dati e
          possono richiederne la modifica o la cancellazione in ogni momento.</p>
        <!-- Titolare -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="titolare">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#titolareCollapse"
                aria-expanded="false" aria-controls="titolareCollapse">
                Titolare del trattamento
                <svg class="arrow arrow-blue arrow-down" data-target="#titolareCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="titolareCollapse" class="privacy-collapse collapse" aria-labelledby="titolare"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Il titolare del trattamento è MAURIZIO GUERMANDI S.R.L. con sede
                legale in Via Bellacosta 26 40137 Bologna, nella persona del Amministratore Unico.</p>
              <p class="privacy-card-text">Indirizzo email del Titolare: info@guermandi.it</p>
              <p class="privacy-card-text">Maurizio Guermandi Srl è di seguito definita come Guermandi Group, l’Agenzia
                o il
                Gruppo.</p>
            </div>
          </div>
        </div>
        <!-- Finalità del Trattamento dei Dati raccolti -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="finalita">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#finalitaCollapse"
                aria-expanded="false" aria-controls="finalitaCollapse">Finalità del Trattamento dei Dati raccolti
                <svg class="arrow arrow-blue arrow-down" data-target="#finalitaCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="finalitaCollapse" class="privacy-collapse collapse" aria-labelledby="finalita"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">I Dati dell’Utente sono raccolti per consentire al Titolare di fornire i
                propri Servizi, così come per le seguenti finalità: <span class="privacy-card-text-highlight">Contattare
                  l’Utente, Interazione con social network e piattaforme esterne, Registrazione ed autenticazione,
                  Accesso agli account su servizi terzi, Gestione dei pagamenti, Monitoraggio dell’infrastruttura,
                  Gestione indirizzi e invio di messaggi email, Statistica, Visualizzazione di contenuti da piattaforme
                  esterne,Affiliazione commerciale, Interazione con le piattaforme di supporto, di feedback e di live
                  chat, Gestione dei database di utenti, Gestione delle richieste di supporto e contatto Hosting ed
                  infrastruttura backend.</span></p>
            </div>
          </div>
        </div>
        <!-- Tipologie di dati -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="tipologieDati">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#tipologieDatiCollapse"
                aria-expanded="false" aria-controls="tipologieDatiCollapse">Tipologie di dati
                <svg class="arrow arrow-blue arrow-down" data-target="#tipologieCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="tipologieDatiCollapse" class="privacy-collapse collapse" aria-labelledby="tipologieDati"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Fra i Dati Personali raccolti da i siti e dalle applicazioni del Gruppo, in
                modo autonomo o tramite terze parti, ci sono: Cookie, Dati di utilizzo, Password, Nome, Cognome, Email,
                Azienda, Numeri di telefono.</p>
              <p class="privacy-card-text">Inoltre sono trattati tutti i dati necessari a fornire i servizi oggetto di
                contratto tra Utente e Titolare. I Dati Personali raccolti per scopi collegati all’esecuzione di un
                contratto tra il Titolare e l’Utente saranno trattati con gli strumenti fisici e digitali utili alla
                realizzazione del servizio richiesto (es. software gestionali e di fatturazione, hosting, CMS, ed altri
                strumenti).</p>
            </div>
          </div>
        </div>
        <!-- Modalità di trattamento -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="modalita">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#modalitaCollapse"
                aria-expanded="false" aria-controls="modalitaCollapse">Modalità di trattamento
                <svg class="arrow arrow-blue arrow-down" data-target="#modalitaCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="modalitaCollapse" class="privacy-collapse collapse" aria-labelledby="modalita"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Il Titolare adotta le opportune misure di sicurezza volte ad impedire
                l’accesso, la divulgazione, la modifica o la distruzione non autorizzate dei Dati Personali.</p>
              <p class="privacy-card-text">Il trattamento viene effettuato mediante strumenti informatici e/o
                telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate.
                Oltre al Titolare, in alcuni casi, potrebbero avere accesso ai Dati altri soggetti coinvolti
                nell’organizzazione dell’Agenzia (personale amministrativo, commerciale, marketing, legali,
                amministratori di sistema) ovvero soggetti esterni (come fornitori di servizi tecnici terzi, corrieri
                postali, hosting provider, società informatiche, agenzie di comunicazione) nominati anche, se
                necessario, Responsabili del Trattamento da parte del Titolare. L’elenco aggiornato dei Responsabili
                potrà sempre essere richiesto al Titolare del Trattamento.</p>
              <h3 class="privacy-card-text-subtitle">Form di contatto</h3>
              <p class="privacy-card-text">In diverse pagine dei siti del gruppo sono presenti form di contatto. Le
                finalità e la tipologia di dati raccolti attraverso i form di contatto sono esplicitati nelle form
                stesse. I dati che l’Utente decide di inviare attraverso questi form restano memorizzati nella posta
                elettronica (<a href="https://policies.google.com/technologies/product-privacy" target="_blank"
                  class="privacy-card-link">Gmail</a>), nel database del sito e nei backup. Se espressamente richiesto
                dall’Utente al
                momento della compilazione del form di contatto, i dati saranno inseriti nella mailing list del Gruppo
                (vedi la voce Newsletter).</p>
              <h3 class="privacy-card-text-subtitle">Social Media</h3>
              <p class="privacy-card-text">Utilizziamo i social media come strumento di contatto con gli utenti e per
                presentare la nostra attività.</p>
              <p class="privacy-card-text">Il nostro utilizzo dei social media è evidenziato all’interno dei siti web
                del Gruppo. Ad esempio, è possibile guardare video che sono caricati sulle nostre pagine YouTube e
                seguire link da e per i nostri account Twitter e Facebook.</p>
              <p class="privacy-card-text">Ogni canale social media ha la propria privacy policy di riferimento valida
                per il processo dei dati personali una volta che si proceda sui loro website. Per esempio, se si sceglie
                di guardare un nostro video su YouTube, la privacy policy di riferimento è quella di <a
                  href="https://www.youtube.com/static?template=privacy_guidelines" target="_blank"
                  class="privacy-card-link">YouTube</a>; se si
                naviga nella nostra attività Twitter su Twitter, sarà necessario accettare la privacy policy di <a
                  href="https://twitter.com/privacy?lang=en" target="_blank" class="privacy-card-link">Twitter</a>,
                lo stesso per <a href="https://www.linkedin.com/legal/privacy-policy" target="_blank"
                  class="privacy-card-link">LinkedIn</a> o <a href="https://www.facebook.com/policy.php" target="_blank"
                  class="privacy-card-link">Facebook</a>.</p>
              <p class="privacy-card-text">Per qualsiasi dubbio sull’uso dei dati, si consiglia di leggere attentamente
                le privacy policy di ogni canale social media prima di utilizzarlo.</p>
              <h3 class="privacy-card-text-subtitle">Newsletter</h3>
              <p class="privacy-card-text">La newsletter del nostro gruppo è gestita con <a
                  href="http://www.mailchimp.com/" target="_blank" class="privacy-card-link">MailChimp</a>, e i dati di
                contatto vengono memorizzati sui server di MailChimp (anche fuori dall’Unione Europea). Periodicamente,
                sono eseguiti backup delle nostre mailing list, conservati nel rispetto delle regole vigenti, con
                strumenti quali <a href="https://policies.google.com/privacy?hl=it" target="_blank"
                  class="privacy-card-link">Drive</a> e CRM.</p>
              <p class="privacy-card-text">
                <a href="http://mailchimp.com/legal/privacy/" target="_blank" class="privacy-card-link">Qui puoi leggere
                  la privacy policy di MailChimp</a>. Notifiche contenenti
                dati di
                contatto potrebbero esserci inviate via posta elettronica, gestita con Thunderbird e <a
                  href="https://policies.google.com/technologies/product-privacy" target="_blank"
                  class="privacy-card-link">Gmail</a>.
              </p>
              <h3 class="privacy-card-text-subtitle">Analisi della navigazione</h3>
              <p class="privacy-card-text">Per fini statistici e per migliorare le funzionalità dei siti del gruppo
                viene utilizzato <a href="http://www.google.com/analytics/" target="_blank"
                  class="privacy-card-link">Google Analytics</a>.</p>
              <p class="privacy-card-text">Google Analytics è impostato in modo che gli indirizzi IP vengano
                anonimizzati, quindi non è possibile risalire alla navigazione di uno specifico utente, ma è utilizzato
                solamente per analizzare dati aggregati.</p>
              <p class="privacy-card-text">Le informazioni generate dal cookie di Google Analytics sull’utilizzo del
                sito web vengono trasmesse a Google e depositate presso server negli Stati Uniti.</p>
              <p class="privacy-card-text">Google potrebbe trasferire queste informazioni a terzi nel caso in cui questo
                sia imposto dalla legge o nel caso in cui si tratti di soggetti che trattano queste informazioni per suo
                conto; <a href="http://www.google.it/intl/it/policies/" target="_blank"
                  class="privacy-card-link">l’informativa su privacy e uso dei dati di Google Analytics è qui</a>.</p>
              <h3 class="privacy-card-text-subtitle">Log di sistema e manutenzione</h3>
              <p class="privacy-card-text">Per necessità legate al funzionamento ed alla manutenzione, i siti e le
                applicazioni del Gruppo e gli eventuali servizi terzi da essa utilizzati potrebbero raccogliere Log di
                sistema, ossia file che registrano le interazioni e che possono contenere anche Dati Personali, quali
                l’indirizzo IP Utente.</p>
            </div>
          </div>
        </div>
        <!-- Base giuridica del trattamento -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="base">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#baseCollapse"
                aria-expanded="false" aria-controls="baseCollapse">Base giuridica del trattamento
                <svg class="arrow arrow-blue arrow-down" data-target="#baseCollapse" xmlns="http://www.w3.org/2000/svg"
                  width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="baseCollapse" class="privacy-collapse collapse" aria-labelledby="base" data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Il Titolare tratta Dati Personali relativi all’Utente in caso sussista una
                delle seguenti condizioni:</p>
              <ul class="privacy-card-text">
                <li>l’Utente ha prestato il consenso per una o più finalità specifiche;</li>
                <li>il trattamento è necessario all’esecuzione di un contratto con l’Utente e/o all’esecuzione di misure
                  precontrattuali;</li>
                <li>il trattamento è necessario per adempiere un obbligo legale al quale è soggetto il Titolare;</li>
                <li>il trattamento è necessario per l’esecuzione di un compito di interesse pubblico o per l’esercizio
                  di pubblici poteri di cui è investito il Titolare;</li>
                <li>il trattamento è necessario per il perseguimento del legittimo interesse del Titolare o di terzi.
                </li>
              </ul>
              <p class="privacy-card-text">E’ comunque sempre possibile richiedere al Titolare di chiarire la concreta
                base giuridica di ciascun trattamento ed in particolare di specificare se il trattamento sia basato
                sulla legge, previsto da un contratto o necessario per concludere un contratto.</p>
            </div>
          </div>
        </div>
        <!-- Luogo -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="luogo">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#luogoCollapse"
                aria-expanded="false" aria-controls="luogoCollapse">Luogo
                <svg class="arrow arrow-blue arrow-down" data-target="#luogoCollapse" xmlns="http://www.w3.org/2000/svg"
                  width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="luogoCollapse" class="privacy-collapse collapse" aria-labelledby="luogo" data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">I Dati sono trattati presso le sedi operative del Titolare ed in ogni altro
                luogo in cui le parti coinvolte nel trattamento siano localizzate. Per ulteriori informazioni, contatta
                il Titolare.</p>
              <p class="privacy-card-text">I Dati Personali dell’Utente potrebbero essere trasferiti in un paese diverso
                da quello in cui l’Utente si trova. Per ottenere ulteriori informazioni sul luogo del trattamento
                l’Utente può fare riferimento alla sezione relativa ai dettagli sul trattamento dei Dati Personali.</p>
              <p class="privacy-card-text">L’Utente ha diritto a ottenere informazioni in merito alla base giuridica del
                trasferimento di Dati al di fuori dell’Unione Europea o ad un’organizzazione internazionale di diritto
                internazionale pubblico o costituita da due o più paesi, come ad esempio l’ONU, nonché in merito alle
                misure di sicurezza adottate dal Titolare per proteggere i Dati.</p>
            </div>
          </div>
        </div>
        <!-- Periodo di conservazione -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="periodo">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#periodoCollapse"
                aria-expanded="false" aria-controls="periodoCollapse">Periodo di conservazione
                <svg class="arrow arrow-blue arrow-down" data-target="#periodoCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="periodoCollapse" class="privacy-collapse collapse" aria-labelledby="periodo"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">I Dati sono trattati e conservati per il tempo richiesto dalle finalità per
                le quali sono stati raccolti.</p>
              <p class="privacy-card-text">Pertanto:</p>
              <ul class="privacy-card-text">
                <li class="privacy-card-text">
                  I Dati Personali raccolti per scopi collegati all’esecuzione di un contratto tra il Titolare e
                  l’Utente saranno trattenuti sino a quando sia completata l’esecuzione di tale contratto.</li>
                <li class="privacy-card-text">I Dati Personali raccolti per finalità riconducibili all’interesse
                  legittimo del Titolare saranno
                  trattenuti sino al soddisfacimento di tale interesse. L’Utente può ottenere ulteriori informazioni in
                  merito all’interesse legittimo perseguito dal Titolare nelle relative sezioni di questo documento o
                  contattando il Titolare.</li>
              </ul>
              <p class="privacy-card-text">Quando il trattamento è basato sul consenso dell’Utente, il Titolare può
                conservare i Dati Personali più a lungo sino a quando detto consenso non venga revocato. Inoltre il
                Titolare potrebbe essere obbligato a conservare i Dati Personali per un periodo più lungo in
                ottemperanza ad un obbligo di legge o per ordine di un’autorità.</p>
              <p class="privacy-card-text">Al termine del periodo di conservazioni i Dati Personali saranno cancellati.
                Pertanto, allo spirare di tale termine il diritto di accesso, cancellazione, rettificazione ed il
                diritto alla portabilità dei Dati non potranno più essere esercitati.</p>
            </div>
          </div>
        </div>
        <!-- Come esercitare i diritti -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="diritti">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#dirittiCollapse"
                aria-expanded="false" aria-controls="dirittiCollapse">Come esercitare i diritti
                <svg class="arrow arrow-blue arrow-down" data-target="#dirittiCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="dirittiCollapse" class="privacy-collapse collapse" aria-labelledby="diritti"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">In qualsiasi momento sarà possibile richiedere gratuitamente la verifica, la
                cancellazione, la modifica dei propri dati, o ricevere l’elenco degli incaricati al trattamento,
                scrivendo una mail a info@guermandi.it. Le richieste saranno evase dal Titolare nel più breve tempo
                possibile, in ogni caso entro un mese.</p>
            </div>
          </div>
        </div>
        <!-- Modifiche a questa privacy policy -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="modifiche">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#modificheCollapse"
                aria-expanded="false" aria-controls="modificheCollapse">Modifiche a questa privacy policy
                <svg class="arrow arrow-blue arrow-down" data-target="#modificheCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="modificheCollapse" class="privacy-collapse collapse" aria-labelledby="modifiche"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Il Titolare del Trattamento si riserva il diritto di apportare modifiche alla
                presente privacy policy in qualunque momento dandone informazione agli Utenti su questa pagina e, se
                possibile, su i siti e le applicazioni del Gruppo nonché, qualora tecnicamente e legalmente fattibile,
                inviando una notifica agli Utenti attraverso uno degli estremi di contatto di cui è in possesso il
                Titolare . Si prega dunque di consultare regolarmente questa pagina, facendo riferimento alla data di
                ultima modifica indicata in fondo.</p>
            </div>
          </div>
        </div>
        <!-- </div> -->
      </div>
      <div class="privacy-col col-12 col-md-6">
        <!-- ----------- -->
        <!-- TITLE RIGHT -->
        <!-- ----------- -->
        <h2 class="privacy-subtitle">Informativa sull’uso dei cookie</h2>
        <p class="privacy-descr">Questo sito web utilizza cookie e tecnologie simili per garantire il corretto
          funzionamento delle procedure e migliorare l’esperienza di uso delle applicazioni online. Di seguito le
          informazioni dettagliate sull’uso dei cookie.</p>
        <!-- <div id="accordionRight" class="privacy-accordion"> -->
        <!-- Definizioni -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="definizioni">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#definizioniCollapse"
                aria-expanded="false" aria-controls="definizioniCollapse">
                Definizioni
                <svg class="arrow arrow-blue arrow-down" data-target="#definizioniCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="definizioniCollapse" class="privacy-collapse collapse" aria-labelledby="definizioni"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">I cookie sono brevi frammenti di testo (lettere e/o numeri) che permettono al
                server web di memorizzare sul client (il browser) informazioni da riutilizzare nel corso della medesima
                visita al sito (cookie di sessione) o in seguito, anche a distanza di giorni (cookie persistenti). I
                cookie vengono memorizzati, in base alle preferenze dell’utente, dal singolo browser sullo specifico
                dispositivo utilizzato (computer, tablet, smartphone).</p>

              <p class="privacy-card-text">Tecnologie similari, come, ad esempio, web beacon, GIF trasparenti e tutte le
                forme di storage locale introdotte con HTML5, sono utilizzabili per raccogliere informazioni sul
                comportamento dell’utente e sull’utilizzo dei servizi.</p>

              <p class="privacy-card-text">Nel seguito di questo documento faremo riferimento ai cookie e a tutte le
                tecnologie similari utilizzando semplicemente il termine “cookie”.</p>
            </div>
          </div>
        </div>
        <!-- Tipologie di cookie -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="tipologie">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#tipologieCollapse"
                aria-expanded="false" aria-controls="tipologieCollapse">
                Tipologie di cookie
                <svg class="arrow arrow-blue arrow-down" data-target="#tipologieCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="tipologieCollapse" class="privacy-collapse collapse" aria-labelledby="tipologie"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">In base alle caratteristiche e all’utilizzo dei cookie possiamo distinguere
                diverse categorie:</p>

              <ul class="privacy-card-text">
                <li class="privacy-card-text">Cookie strettamente necessari (tecnici). Si tratta di cookie
                  indispensabili per il corretto
                  funzionamento del nostro sito. La durata dei cookie è strettamente limitata alla sessione di lavoro
                  (chiuso il browser vengono cancellati).</li>
                <li class="privacy-card-text">Cookie di analisi e prestazioni. Sono cookie utilizzati per raccogliere e
                  analizzare il traffico e
                  l’utilizzo del sito in modo anonimo. Questi cookie, pur senza identificare l’utente, consentono, per
                  esempio, di rilevare se il medesimo utente torna a collegarsi in momenti diversi. Permettono inoltre
                  di monitorare il sistema e migliorarne le prestazioni e l’usabilità. La disattivazione di tali cookie
                  può essere eseguita senza alcuna perdita di funzionalità.</li>
                <li class="privacy-card-text">Cookie di profilazione. Si tratta di cookie permanenti utilizzati per
                  identificare (in modo anonimo
                  e non) le preferenze dell’utente e migliorare la sua esperienza di navigazione. Il nostro sito non
                  utilizzano cookie di questo tipo.</li>
              </ul>
            </div>
          </div>
        </div>
        <!-- Cookie di terze parti -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="terze">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#terzeCollapse"
                aria-expanded="false" aria-controls="terzeCollapse">
                Cookie di terze parti
                <svg class="arrow arrow-blue arrow-down" data-target="#terzeCollapse" xmlns="http://www.w3.org/2000/svg"
                  width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="terzeCollapse" class="privacy-collapse collapse" aria-labelledby="terze" data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Visitando un sito web si possono ricevere cookie sia dal sito visitato
                (“proprietari”), sia da siti gestiti da altre organizzazioni (“terze parti”). Un esempio è rappresentato
                dalla presenza dei “social plugin” per Facebook, Twitter, Google+ e LinkedIn. Si tratta di parti della
                pagina visitata generate direttamente dai suddetti siti ed integrati nella pagina del sito ospitante.
                L’utilizzo più comune dei social plugin è finalizzato alla condivisione dei contenuti sui social
                network.</p>

              <p class="privacy-card-text">La presenza di questi plugin comporta la trasmissione di cookie da e verso
                tutti i siti gestiti da terze parti. La gestione delle informazioni raccolte da “terze parti” è
                disciplinata dalle relative informative cui si prega di fare riferimento. Per garantire una maggiore
                trasparenza e comodità, si riportano qui di seguito gli indirizzi web delle diverse informative e delle
                modalità per la gestione dei cookie.</p>

              <p><a href="https://www.facebook.com/help/cookies/" target="_blank" class="privacy-card-link">Facebook
                  informativa</a></p>

              <p class="privacy-card-text">Facebook (configurazione): accedere al proprio account. Sezione privacy.</p>

              <p><a href="https://support.twitter.com/articles/20170514" target="_blank"
                  class="privacy-card-link">Twitter
                  informative</a></p>

              <p><a href="https://twitter.com/settings/security" target="_blank" class="privacy-card-link">Twitter
                  (configurazione)</a></p>

              <p><a href="https://www.linkedin.com/legal/cookie-policy" target="_blank"
                  class="privacy-card-link">Linkedin
                  informativa</a></p>

              <p><a href="https://www.linkedin.com/settings/" target="_blank" class="privacy-card-link">Linkedin
                  (configurazione)</a></p>

              <p><a href="http://www.google.it/intl/it/policies/technologies/cookies/" target="_blank"
                  class="privacy-card-link">Google+ informativa</a></p>

              <p><a href="http://www.google.it/intl/it/policies/technologies/managing/" target="_blank"
                  class="privacy-card-link">Google+ (configurazione)</a></p>
            </div>
          </div>
        </div>
        <!-- Google Analytics -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="analytics">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#analyticsCollapse"
                aria-expanded="false" aria-controls="analyticsCollapse">
                Google Analytics
                <svg class="arrow arrow-blue arrow-down" data-target="#analyticsCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="analyticsCollapse" class="privacy-collapse collapse" aria-labelledby="analytics"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Questo sito include anche talune componenti trasmesse da Google Analytics, un
                servizio di analisi del traffico web fornito da Google, Inc. (“Google”). Anche in questo caso si tratta
                di cookie di terze parti raccolti e gestiti in modo anonimo per monitorare e migliorare le prestazioni
                del sito ospite (performance cookie).</p>
              <p class="privacy-card-text">Google Analytics utilizza i “cookie” per raccogliere e analizzare in forma
                anonima le informazioni sui comportamenti di utilizzo del sito. Tali informazioni vengono raccolte da
                Google Analytics, che le elabora allo scopo di redigere report riguardanti le attività sui siti web
                stessi. Questo sito non utilizza (e non consente a terzi di utilizzare) lo strumento di analisi di
                Google per monitorare o per raccogliere informazioni personali di identificazione. Google non associa
                l’indirizzo IP a nessun altro dato posseduto da Google né cerca di collegare un indirizzo IP con
                l’identità di un utente. Google può anche comunicare queste informazioni a terzi ove ciò sia imposto
                dalla legge o laddove tali terzi trattino le suddette informazioni per conto di Google.</p>
              <p class="privacy-card-text">Per ulteriori informazioni, si rinvia a questo <a
                  href="https://www.google.it/policies/privacy/partners/" target="_blank"
                  class="privacy-card-link">link</a>.</p>
              <p class="privacy-card-text">L’utente può disabilitare in modo selettivo l’azione di Google Analytics
                installando sul proprio browser la componente di opt-out fornito da Google. Per disabilitare l’azione di
                Google Analytics, si rinvia a questo <a href="https://tools.google.com/dlpage/gaoptout" target="_blank"
                  class="privacy-card-link">link.</a></p>
            </div>
          </div>
        </div>
        <!-- Durata dei cookie -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="durata">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#durataCollapse"
                aria-expanded="false" aria-controls="durataCollapse">
                Durata dei cookie
                <svg class="arrow arrow-blue arrow-down" data-target="#durataCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="durataCollapse" class="privacy-collapse collapse" aria-labelledby="durata" data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">Questo sito non utilizza cookie persistenti ad eccezione dei cookie di Google
                Analytics. Di seguito le durate dei cookie di Google Analytics:</p>

              <p class="privacy-card-text">
              <ul class="privacy-card-text-list">
                <li class="privacy-card-text">__utma => 2 anni</li>

                <li class="privacy-card-text">__utmb => 30 minuti</li>

                <li class="privacy-card-text">__utmc => Sessione</li>

                <li class="privacy-card-text">__utmt => 10 minuti</li>

                <li class="privacy-card-text">__utmz => 6 mesi</li>

                <li class="privacy-card-text">_ga => 2 anni</li>

                <li class="privacy-card-text">_gat => 10 minuti</li>
              </ul>
              </p>
            </div>
          </div>
        </div>
        <!-- Gestione dei cookie -->
        <div class="privacy-card card">
          <div class="privacy-card-header card-header" id="gestione">
            <h5 class="mb-0">
              <button class="privacy-btn btn btn-link" data-toggle="collapse" data-target="#gestioneCollapse"
                aria-expanded="false" aria-controls="gestioneCollapse">
                Gestione dei cookie
                <svg class="arrow arrow-blue arrow-down" data-target="#gestioneCollapse"
                  xmlns="http://www.w3.org/2000/svg" width="28.281" height="24.553" viewBox="0 0 28.281 24.553">
                  <g transform="translate(42.422 -30.003) rotate(90)">
                    <g transform="translate(30.003 26.31)">
                      <rect width="21.997" height="4" fill="#0094d8" />
                    </g>
                    <g transform="translate(23.444)">
                      <g transform="translate(14.14 39.594) rotate(-45)">
                        <rect width="20.003" height="4" fill="#0094d8" />
                      </g>
                      <g transform="translate(16.97 14.142) rotate(45)">
                        <rect width="20.001" height="4" fill="#0094d8" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
            </h5>
          </div>
          <div id="gestioneCollapse" class="privacy-collapse collapse" aria-labelledby="gestione"
            data-parent="#accordion">
            <div class="privacy-card-body card-body">
              <p class="privacy-card-text">L’utente può decidere se accettare o meno i cookie utilizzando le
                impostazioni del proprio browser.</p>

              <p class="privacy-card-text">Attenzione: la disabilitazione totale o parziale dei cookie tecnici può
                compromettere l’utilizzo delle funzionalità del sito riservate agli utenti registrati. Al contrario, la
                fruibilità dei contenuti pubblici è possibile anche disabilitando completamente i cookie.</p>

              <p class="privacy-card-text">La disabilitazione dei cookie “terze parti” non pregiudica in alcun modo la
                navigabilità.</p>

              <p class="privacy-card-text">L’impostazione può essere definita in modo specifico per i diversi siti e
                applicazioni web. Inoltre i migliori browser consentono di definire impostazioni diverse per i cookie
                “proprietari” e per quelli di “terze parti”.</p>

              <p class="privacy-card-text">A titolo di esempio, in Firefox, attraverso il menu Strumenti->Opzioni
                ->Privacy, è possibile accedere ad un pannello di controllo dove è possibile definire se accettare o
                meno i diversi tipi di cookie e procedere alla loro rimozione.</p>

              <p class="privacy-card-text"><a href="https://support.google.com/chrome/answer/95647?hl=it"
                  target="_blank" class="privacy-card-link">Chrome</a></p>

              <p class="privacy-card-text"><a href="https://support.mozilla.org/it/kb/Gestione%20dei%20cookie"
                  target="_blank" class="privacy-card-link">Firefox</a></p>

              <p class="privacy-card-text"><a
                  href="http://windows.microsoft.com/it-it/windows7/how-to-manage-cookies-in-internet-explorer-9"
                  target="_blank" class="privacy-card-link">Internet Explorer</a></p>

              <p class="privacy-card-text"><a href="http://help.opera.com/Windows/10.00/it/cookies.html" target="_blank"
                  class="privacy-card-link">Opera</a></p>

              <p class="privacy-card-text"><a href="http://support.apple.com/kb/HT1677?viewlocale=it_IT" target="_blank"
                  class="privacy-card-link">Safari</a></p>
            </div>
          </div>
        </div>
        <!-- </div> -->
      </div>
    </div>
  </div>
</section>