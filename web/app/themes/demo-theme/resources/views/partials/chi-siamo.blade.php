<section id="about" class="about container-fluid">
  {{-- <div class="about-bg"> --}}
    {{-- <img src="@asset('images/consumabile-brochure.webp')" alt="" class="about-bg-img"> --}}
    {{-- </div> --}}
@php $description = get_field('chi_siamo'); @endphp
@php $cta = get_field('cta') @endphp
  <div class="spacer-16"></div>
  <div class="spacer-64 mobile-spacer"></div>
  <div class="about-wrapper-new">
  {{-- <div> --}}
    @if (is_array($description) || is_object($description))
    @foreach ($description as $descr)
      <h1 class="workflow-box-description-title pseudo-a-nobefore lh10">{{ $descr['line'] }}</h1>
    @endforeach
    @endif
    {{-- </div> --}}
    <div class="spacer2">
    </div>
    {{-- <div class="about-cta d-grid"> --}}
      <button type="button" class="cta">
        <a href="" class="cta-link paleorange">
          {{ $cta }}
        </a>
      </button>
    {{-- </div> --}}
  </div>
</section>