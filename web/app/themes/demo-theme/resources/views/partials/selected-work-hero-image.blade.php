<?php $hero = get_field('hero_image'); ?>
<section class="container-fluid caso-hero">
  <?php if( !empty( $hero ) ): ?>
  <img src="<?php echo esc_url($hero['url']); ?>" width="100%" class="caso-hero-image"  alt="<?php echo esc_attr($hero['alt']); ?>" />
<?php endif; ?>
@if(!$hero)
<div class="spacer-32"></div>
<div class="spacer-32"></div>
@endif
</section>
