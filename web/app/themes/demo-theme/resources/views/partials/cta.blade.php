@while(have_posts()) @php the_post() @endphp
@php $motto = get_field('agency_motto') @endphp
@php $cta = get_field('cta') @endphp
<section class="cta container">
  <div class="spacer-12"></div>
  <div class="cta-row">
    <h3 class="about-text">{{ get_bloginfo('description') }}</h3>
  </div>
  <div class="cta-row">
    <button type="button" class="cta">
      <a href="/" class="workflow-box-description-title lh10">
        {{ $cta }}
      </a>
    </button>
  </div>
  <div class="spacer-32 mobile-spacer"></div>
</section>
@endwhile