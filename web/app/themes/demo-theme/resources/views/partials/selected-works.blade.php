@php
$args = [
  'posts_per_page' => -1,
  'post_type' => 'selected_work',
];
$selectedWorksList = new WP_Query($args);
@endphp
<section class="case">
  <div class="spacer-8"></div>
  <div class="case-wrapper container mx-auto">
    <div class="case-headline row">
      <h2 class="workflow-box-description-title pink">Selected Works</h2>
    </div>
    <div class="case-row row">
      <!---------->
      <!-- LIST -->
      <!---------->
      <div class="case-col col-12 col-md-6 px-2">
        {{-- LOOP EVEN --}}
<?php
while($selectedWorksList->have_posts()) {
  $selectedWorksList->the_post();
  $even = false;
  if ($selectedWorksList->current_post % 2 == 0) {
    $even = true;
  } 
?>
<?php if ($even) { ?>
        <div class="case-item border-bottom mx-2 w-100">            
          <button data-case="tecna" class="case-list-item-button">
            <div>
              <a class="case-list-item-button-link" href="{{ the_permalink() }}">
                <?php the_title(); ?>
              </a>
            </div>
            <div>
              <p class="case-list-item-button-sub">
                {{ get_field('motto') }}
              </p>
            </div>
          </button>                    
        </div>
<?php }} ?>
@php(wp_reset_postdata())
      </div>
      <!-- LIST -->
      <div class="case-col col-12 col-md-6 px-2">
        {{-- LOOP ODD --}}
<?php
while($selectedWorksList->have_posts()) {
$selectedWorksList->the_post();
$even = false;
if ($selectedWorksList->current_post % 2 == 0) {
  $even = true;
} 
if (!$even) { ?>
        <div class="case-item border-bottom mx-2 w-100">
          <button data-case="scoa" class="case-list-item-button">
            <div>
              <a class="case-list-item-button-link" href="{{ the_permalink() }}">
                <?php the_title(); ?>
              </a>
            </div>
            <div>
                <p class="case-list-item-button-sub">{{ get_field('motto') }}</p>
            </div>
          </button>
        </div>
<?php }} ?>
@php(wp_reset_postdata())
      </div>
    </div>
  </div>
</section>