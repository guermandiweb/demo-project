<section id="gruppo" class="gruppo">
  <div class="gruppo container">
    <div class="gruppo-row gruppo-headline row">
      <h2 class="gruppo-headline-title">Il gruppo</h2>
    </div>
    <div class="gruppo-row row">
      <div class="gruppo-col-descr col-6">
        <h3 class="gruppo-col-descr-motto">We talk <br>
          <span class="pink">your language</span>
        </h3>
        <img class="gruppo-col-descr-img" src="@asset('images/glocal-audio-logo.png')">
      </div>
      <div class="col-6">
        <img class="gruppo-col-img" src="@asset('images/glocal-audio.webp')">
      </div>
    </div>
    <div class="gruppo-row row">
      <div class="col-6">
        <img class="gruppo-col-img" src="@asset('images/verso-learn-img.webp')">
      </div>
      <div class="gruppo-col-descr col-6">
        <h3 class="gruppo-col-descr-motto">Independent <br>
          <span class="pink">learning</span>
        </h3>
        <img class="gruppo-col-descr-img" src="@asset('images/verso-learn-logo.png')">
      </div>
    </div>
  </div>
</section>