<div id="caso-title">
  <section id="main" class="caso container">
    <div class="spacer-16"></div>
    <div class="spacer-32 d-block d-md-none"></div>
    <div class="caso-row row">
      <div class="caso-col col-12 col-md-4">
        <h2 class="caso-col-title-sub">Parliamo di</h2>
      </div>
      <div class="caso-col col-12 col-md-8 border-left pb-5">
        <h1 class="caso-col-title caso-col-title-big">{{ the_title() }}<br></h1>
        <h2 class="caso-col-title-shortdescr">{{ get_field('motto') }}</h2>
      </div>
    </div>
  </section>
</div>