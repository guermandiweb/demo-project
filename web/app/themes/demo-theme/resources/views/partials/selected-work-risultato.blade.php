@php
$results = get_field('numbers')
@endphp
@if($results)
<section id="main" class="caso container">
  <div class="spacer-16"></div>
  <div class="caso-row row">
    <div class="caso-col col-12 col-md-4">
      <h2 class="caso-col-title-sub">Risultati</h2>
    </div>
    <div class="col-12 col-md-8 border-left pr-0-on-mobile row">
      @foreach($results as $result)
      <div class="caso-col-grid col-md-6">
        <h2 class="caso-col-highlight">{{ $result['number'] }}</h2>
        <p class="caso-col-text">{{ $result['result'] }}</p>
      </div>
      @endforeach
      <div class="caso-col col-12 pl-on-mobile">
        <p class="caso-col-descr">{{ get_field('briefly') }}
        </p>
      </div>
    </div>
  </div>
</section>
@endif