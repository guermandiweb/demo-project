@php
$workflow = get_field('workflow')
@endphp
@if ($workflow)
<section id="workflow" class="workflow container-fluid">
  {{-- <div class="spacer-8"></div> --}}
  @foreach ($workflow as $step)
  <div class="workflow-box row">
    <div class="workflow-box-icon col-12 col-md-3">
      @php $image = $step['icon'] @endphp
      <?php if( !empty( $image ) ): ?>
      <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
    @endif
    </div>
    <div class="workflow-box-description col-12 col-md-8">
      <div class="mr-auto">
        <h3 class="workflow-box-description-title pseudo-a">{{ $step['title'] }}</h3>
      </div>
      <p class="workflow-box-description-text">{{ $step['descr'] }}</p>
    </div>
  </div>
  @endforeach
  <div class="spacer"></div>
</section>
@endif