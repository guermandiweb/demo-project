@php
$paragraphs = get_field('paragraphs')
@endphp
@if($paragraphs)
<section id="main" class="caso container">
  <div class="spacer-16"></div>
  <div class="caso-row row">
    <div class="caso-col col-12 col-md-4">
      <h2 class="caso-col-title-sub">Intro</h2>
    </div>
    <div class="caso-col col-12 col-md-8 border-left">
@foreach($paragraphs as $paragraph)
      <p class="caso-col-descr">{{ $paragraph['introduction'] }}</p>
@endforeach
    </div>
  </div>
</section>
@endif