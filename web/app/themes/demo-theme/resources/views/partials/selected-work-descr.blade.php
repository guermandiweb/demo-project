@php $works = get_field('descr') @endphp
@php $heroimage2 = get_field('image2') @endphp
@php $heroimage2descr = get_field('image2_description') @endphp
@php $notfullwidth2 = get_field('not_full_width_2') @endphp

@if($heroimage2)
<section class="container-fluid caso-video<?php
  echo $notfullwidth2 ? ' container' : ''
?>">
  <img src="<?php echo esc_url($heroimage2['url']); ?>" width="100%" class="caso-hero-image-secondary"  alt="<?php echo esc_attr($heroimage2['alt']); ?>" />
  @if($heroimage2descr)
    <div class="about-wrapper-descr-selected-work">
      <p class="about-text-selected-work"><strong>{{ $heroimage2descr }}</strong></p>
    </div>
    @endif
</section>
@endif
@if($works)
@foreach($works as $work)
<section id="descr" class="caso container black">
  <div class="spacer-16 black"></div>
  <div class="caso-row row black">
    <div class="caso-col col-12 col-md-4 black">
      <h2 class="caso-col-title-sub">{{ $work['title'] }}</h2>
    </div>
    <div class="caso-col col-12 col-md-8 border-left black">
      @php $descr = $work['descr_field'] @endphp
      @foreach($descr as $par)
      <p class="caso-col-descr">{{ $par['descr_paragraph'] }}</p>
      @endforeach
      @php $keywords = $work['keywords_field'] @endphp
      <p class="caso-col-descr-keywords">
        @foreach($keywords as $word)
        {{ $word['keywords_text'] }}{!! "&nbsp;" !!}
        @endforeach
      </p>
    </div>
  </div>
</section>

{{-- -------------- --}}
{{-- SECTION SLIDER --}}
{{-- -------------- --}}
@php $gallery1 = $work['gallery'];
@endphp
@if($gallery1)
@foreach ($gallery1 as $image)
@php $notfullwidth = $image['not_full_width'] @endphp
@php $imagedescr = $image['gallery1_image_description'] @endphp
<section class="container-fluid caso-video<?php
  echo $notfullwidth ? ' container' : ''
?>">
  <img src="<?php echo esc_url($image['gallery1_image']['url']); ?>" width="100%" class="caso-hero-image-secondary"  alt="<?php echo esc_attr($image['gallery1_image_description']); ?>" />
  @if($imagedescr)
    <div class="about-wrapper-descr-selected-work">
      <p class="about-text-selected-work"><strong>{{ $imagedescr }}</strong></p>
    </div>
    @endif
</section>
@endforeach
@endif
@endforeach
@endif