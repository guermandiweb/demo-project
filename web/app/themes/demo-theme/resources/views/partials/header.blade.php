<header>
  <div class="nav">
    <div class="nav-brand">
      <div class="nav-brand-logo">
        <a id="brand-logo" class="nav-brand-logo-img" href="{{ home_url('/') }}"><img
            src="@asset('images/logo.svg')"></a>
      </div>
      <div class="nav-brand-name">
        <a class="nav-brand-name-link" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
      </div>
    </div>
    <div id="burger" class="nav-burger">
      <div class="nav-burger-top"></div>
      <div class="nav-burger-bottom"></div>
    </div>
  </div>
</header>
<nav class="nav-menu">
  @if (has_nav_menu('primary_navigation'))
  {!! wp_nav_menu([
  'theme_location' => 'primary_navigation',
  'menu_class' => 'nav-menu-list'
  ]) !!}
  @endif

    @php dynamic_sidebar('sidebar-primary') @endphp
</nav>
