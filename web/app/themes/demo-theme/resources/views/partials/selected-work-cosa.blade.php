@php
$keywords = get_field('list')
@endphp
@if($keywords)
<section id="cosa" class="caso container">
  <div class="spacer-16"></div>
  <div class="caso-row row">
    <div class="caso-col col-12 col-md-4">
      <h2 class="caso-col-title-sub">Cosa</h2>
    </div>
    <div class="caso-col col-12 col-md-8 border-left">
      <ul class="caso-col-descr">
        @foreach($keywords as $keyword)
        <li>{{ $keyword['item'] }}</li>
        {{-- <li>Design</li>
        <li>Digitale</li>
        <li>Contenuti</li>
        <li>Eventi</li> --}}
        @endforeach
      </ul>
    </div>
  </div>
</section>
@endif