<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
   <div class="modal-content">  
    <div class="modal-body">
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           {{-- <span aria-hidden="true">&times;</span> --}}
           <div class="closevideo">
            <div class="closevideo-line1"></div>
            <div class="closevideo-line2"></div>
           </div>
        </button>        
      <!-- 16:9 aspect ratio -->
     <div class="embed-responsive embed-responsive-16by9">
       <iframe style="background-color: #1e2122;" class="vimeo-iframe" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
      </div> 
    </div>
   </div>
  </div>
 </div>