<section id="check" class="check min-vw-100">
  <div id="form" class="check-wrapper container">
    <div class="check-headline row">
      <h1 class="check-headline-title">Contattaci</h1>
    </div>
    <div class="check-row row">
      <?php echo do_shortcode('[contact-form-7 id="118" title="Contact Form 2022"]') ?>
    </div>
  </div>
</section>
{{-- @include('partials.video-check') --}}