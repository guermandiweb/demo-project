{{--
  Title: Hero
  --}}
@php $heroimages = get_field('hero_images') @endphp
@php $service1 = get_field('service1') @endphp
@php $service2 = get_field('service2') @endphp
{{-- @php $service3 = get_field('service3') @endphp
@php $service4 = get_field('service4') @endphp --}}
@php $service5 = get_field('service5') @endphp
@php $service6 = get_field('service6') @endphp

<section id="heronew" class="section topper heronew about">
  <div id="subject" class="heronew-subject">
    @foreach($heroimages as $img)
    @php $hero = $img['hero_image'] @endphp
    <img src="{{ $hero['url'] }}" alt="" class="heronew-subject-img hide">
    @endforeach
    {{-- <img src="@asset('images/15_2022-paresa-mockup-web-products.jpg')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/16_2022-paresa-mockup-web-home.jpg')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/17_mockup_a4_brochure_copertina.jpeg')" alt="" class="heronew-subject-img hide"> --}}

    {{-- <picture class="heronew-subject-img hide"> --}}
      {{-- <source srcset="@asset('images/17_mockup_a4_brochure_copertina.jpeg')" media="(min-width: 800px)"> --}}
      {{-- <img src="@asset('images/pollution_800_responsive_pic_test.jpg')" alt="" />
    </picture> --}}
    {{-- @endforeach --}}
    <!-- <img src="@asset('images/17_mockup_a4_brochure_copertina.jpeg')" alt="" class="heronew-subject-img hide"> -->
    <!-- <img src="@asset('images/03_hero_atersir-sito-mockup.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/04_hero_b2beasy-app.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/05_hero_gg-ebi-lineeguida.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/06_hero_imac_tecna-02.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/07_hero_rigosi-mockup-02.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/08_hero_scoa-segnalibro.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/09_hero_turolla_poster_bauma_behance.jpg')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/10_hero_yama-mockup-loghi.webp')" alt="" class="heronew-subject-img hide">
    <img src="@asset('images/11_hero_yama-mockup-logo.webp')" alt="" class="heronew-subject-img hide"> -->
  </div>
  <div class="about-wrapper-descr">
    <div class="about-cta d-grid">
      <p class="about-text"><strong>{{ $motto }}</strong></p>
      <div class="spacer"></div>
      <div class="pb-5">
        
        <button type="button" class="hero-content-button-link">
          <a href="" class="hero-content-button-link orange">
            Parlaci del tuo progetto
          </a>
        </button>
      </div>
      <div>
        <button type="button" class="hero-content-button-link">
          <a href="" data-toggle="modal" data-src="https://player.vimeo.com/video/58385453?badge=0&autoplay=1&loop=1" data-target="#myModal" class="hero-content-button-link orange secondary-link">
            Guarda lo showreel
          </a>
        </button>
      </div>
    </div>
  </div>
</div>
<section class="section marquee">
  <div class="marquee-track l">
      {!! $service1 !!}
      </div>
  <div class="marquee-track r">
      {!! $service2 !!}
  </div>
  <div class="marquee-track l">
      {!! $service5 !!}
  </div>
  <div class="marquee-track r">
      {!! $service6 !!}
  </div>
</section>
{{-- @include('partials.modal') --}}
