@php $video = get_field('video_url') @endphp
@php $videoid = substr($video, 30) @endphp
@php $videodescr = get_field('video_description') @endphp
@php $heroimage1descr = get_field('image1_description') @endphp
@php $embedsettings = $video.'?rel=0&amp;autoplay=1&mute=1&loop=1&controls=0&modestbranding=1&showinfo=0&playlist='.$videoid @endphp
@php $heroimage1 = get_field('image1') @endphp

@if($video)
  <section class="container-fluid caso-video">
    
      {{-- <iframe class="video" src="{{ $embedsettings }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
      <object class="video" data='{{ $embedsettings }}'>
      </object>
      @if($videodescr)
      <div class="about-wrapper-descr-selected-work">
        <p class="about-text-selected-work"><strong>{{ $videodescr }}</strong></p>
      </div>
      @endif
  </section>
@endif

@if($heroimage1)
<section class="container-fluid">
  <div class="caso-video">

    <img src="<?php echo esc_url($heroimage1['url']); ?>" width="100%" class="caso-hero-image-secondary"  alt="<?php echo esc_attr($heroimage1['alt']); ?>" />
    @if($heroimage1descr)
    <div class="about-wrapper-descr-selected-work">
      <p class="about-text-selected-work"><strong>{{ $heroimage1descr }}</strong></p>
    </div>
    @endif
  </div>
</section>
@endif