@php $testimonials = get_field('testimonials') @endphp
@php $heroimage3 = get_field('image3') @endphp

@if($heroimage3)
<section class="container-fluid">
  <img src="<?php echo esc_url($heroimage3['url']); ?>" width="100%" class="caso-hero-image-secondary"  alt="<?php echo esc_attr($heroimage3['alt']); ?>" />
</section>
@endif
@if($testimonials)
@foreach($testimonials as $review)
<section id="main" class="caso container">
 <div class="spacer-16"></div>
 <div class="caso-row row">
  <div class="caso-blockquote">
   <div class="caso-blockquote-text">
    <q>{{ $review['paragraph'] }}</q>
   </div>
   <div class="caso-blockquote-author">
    {{ $review['name'] }}
   </div>
   <div class="caso-blockquote-role">
    {{ $review['position'] }}
   </div>
  </div>
 </div>
</section>
@endforeach
@endif