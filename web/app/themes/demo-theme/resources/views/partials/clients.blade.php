@php
$args = [
  'post_type' => 'list',
];
$clientsList = new WP_Query($args);
@endphp
<?php
while($clientsList->have_posts()) {
  $clientsList->the_post();
}
?>
@php $clients = get_field('clients') @endphp

@if($clients)
<section class="clients">
  <div class="container clients-wrapper">
    <div class="clients-headline row">
      <h1 class="workflow-box-description-title pink"><?php the_title(); ?></h1>
    </div>
    <div class="clients-row row">
      @if (is_front_page())
      <?php for($i = 0; $i <= 9; $i++) { ?>
      {{-- //@foreach($clients as $client) --}}
      <div class="clients-col col-2 mx-auto text-center">
        @php $logo = $clients[$i]['logo'] @endphp
        <img class="clients-img" src="{{ $logo['url'] }}" alt="{{ $logo['alt'] }}">
      </div>
      <?php } ?>
      @else
      @foreach($clients as $client)
      <div class="clients-col col-2 mx-auto text-center">
        @php $logo = $client['logo'] @endphp
        <img class="clients-img" src="{{ $logo['url'] }}" alt="{{ $logo['alt'] }}">
      </div>
      @endforeach
      @endif
    </div>
  </div>
</section>
@endif