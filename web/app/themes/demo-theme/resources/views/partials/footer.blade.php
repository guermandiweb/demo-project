<footer class="footer min-vw-100">
  <div class="footer-container container">
    <div class="row">
      <div class="footer-widget col-12 col-sm-6 col-lg-4">
          @php dynamic_sidebar('sidebar-footer1') @endphp
      </div>
      <div class="footer-widget col-12 col-sm-6 col-lg-4">
          @php dynamic_sidebar('sidebar-footer2') @endphp
      </div>
      <div class="footer-widget col-12 col-sm-6 col-lg-4">
          @php dynamic_sidebar('sidebar-footer4') @endphp
      </div>
    </div>
  </div>
  @include('partials.newsletter')
</footer>