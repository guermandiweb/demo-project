<?php

global $query_string;

wp_parse_str( $query_string, $search_query );
$search = new WP_Query( $search_query );

?>
@extends('layouts.app')

@section('content')
  {{-- @include('partials.page-header') --}}
  @if (!have_posts())
    <div class="area404">
      <p class="about-text at-angle"><strong>Sorry, but the page you were trying to view <span class="case-list-item-button-link pink">does not exist</span>...</strong></p>
      {!! get_search_form(false) !!}
    </div>
  @endif
@endsection
