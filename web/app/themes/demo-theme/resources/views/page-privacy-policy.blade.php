{{--
  Template Name: Page Privacy Policy
  --}}

@extends('layouts.app')
@section('content')
@while(have_posts()) @php the_post() @endphp
@include('partials.privacy-policy')
@endwhile
@endsection