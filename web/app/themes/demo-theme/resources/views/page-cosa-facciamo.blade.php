{{--
  Template Name: Page Cosa Facciamo
  --}}

@extends('layouts.app')
@section('content')
@while(have_posts()) @php the_post() @endphp
@include('partials.cosa-facciamo-hero')
@include('partials.marketing')
@include('partials.design')
@include('partials.comunicazione')
@include('partials.audiovideo')
@include('partials.clients')
@endwhile
@endsection