@extends('layouts.app')
@php
$args = [
  'posts_per_page' => -1,
  'post_type' => 'post',
];
$posts = new WP_Query($args);
@endphp
<div class="spacer-16"></div>
@section('content')

  <div class="blog-container">
    <div class="blog-row row">
      <?php while($posts->have_posts()) { $posts->the_post(); ?>
      <div class="blog-col col-12 col-sm-6 col-md-4 col-lg-3">
        <div class="article-img" style="background: url('<?php the_post_thumbnail_url(); ?>') no-repeat center center/cover;">
        </div>
        <div class="blog-title">
          <?php
          $title = get_the_title();
    ?>
          <h1 class="caso-col-title-sub"><a href="{{ the_permalink() }}">
            <?php
            $title = get_the_title();
            $length = 45;
            $minlength = 20;
      if ( strlen($title) > $length ) {
          $titleexcerpt = substr($title, 0, $length);
          echo $titleexcerpt.'...';
      } else {
        the_title();
      }
      ?>
          </a></h1>
          <div class="blog-date">
            <p class="orange">{{ the_date() }}</p>
          </div>
        </div>
        <div class="spacer"></div>
        <div class="blog-img">
          
        </div>
        <div class="blog-content">
          <p class="workflow-box-description-text">{{ the_excerpt() }}</p>
        </div>
        <div class="spacer-8"></div>
      </div>
      <?php } ?>
      @php(wp_reset_postdata())
    </div>
  </div>
  
@endsection