@extends('layouts.app')

<div class="blog-item container">
  <div class="spacer-32"></div>
  <a class="" href="{{ esc_url(get_page_link(7690)) }}">Torna alle News</a>
  <div class="blog-item-body">
    <h1 class="blog-item-title">{{ the_title() }}</h1>
    <div class="blog-item-main">
      {{ the_content() }}
      </div>
  </div>
</div>