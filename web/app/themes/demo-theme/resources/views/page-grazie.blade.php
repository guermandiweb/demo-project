@extends('layouts.app')

@section('content')
  <div style="width: 100vw; height: 100vh; display: flex; flex-direction: column; justify-content: center; align-items: center;">
    <p class="caso-col-title caso-col-title-big approaching"><strong><span class="">Grazie!</span></strong></p>
    <p class="about-text">Abbiamo ricevuto la tua email e ti risponderemo al più presto.</p>
  </div>
@endsection