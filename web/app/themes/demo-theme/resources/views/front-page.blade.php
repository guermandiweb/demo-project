{{--
  Template Name: Front Page
  --}}
@php $motto = get_field('agency_motto') @endphp
@extends('layouts.app')
@section('content')
@include('partials.hero-video')
@include('partials.clients')
@include('partials.selected-works')
@include('partials.cta')
@endsection