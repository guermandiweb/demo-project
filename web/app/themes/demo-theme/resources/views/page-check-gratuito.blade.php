{{--
  Template Name: Page Check Gratuito
  --}}

@extends('layouts.app')
@section('content')
@while(have_posts()) @php the_post() @endphp
@include('partials.check-gratuito-hero')
@include('partials.check-video')
@include('partials.check-gratuito')
@endwhile
@endsection