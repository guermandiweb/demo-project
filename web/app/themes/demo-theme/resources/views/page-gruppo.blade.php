{{--
  Template Name: Page Gruppo
  --}}

@extends('layouts.app')
@section('content')
@while(have_posts()) @php the_post() @endphp
@include('partials.gruppo')
@endwhile
@endsection