<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$args = [
	'position' => 'acf_after_title',
	'show_in_rest' => true,
];

$works = new FieldsBuilder('gruppo_selected_works', $args);

$works
		->setLocation('post_type', '==', 'selected_work');

$works
->addFields(get_field_partial('partials.subfields.hero'))
	// INTRO
->addFields(get_field_partial('partials.subfields.intro'))
	// VIDEO
->addFields(get_field_partial('partials.subfields.video'))
	// SECTION IMAGE 1
->addFields(get_field_partial('partials.subfields.image1'))
	// WHAT
->addFields(get_field_partial('partials.subfields.what'))
	
	// SECTION HERO 2
->addFields(get_field_partial('partials.subfields.image2'))
	// WHAT DESCR
	->addRepeater('descr', [
		'label' => 'Description',
	])
		// TITLE
		->addText('title', [
			'label' => 'Title',
		])
		// DESCRIPTION
		->addFields(get_field_partial('partials.subfields.descriptionpar'))
		// KEYWORDS
		->addFields(get_field_partial('partials.subfields.descriptionkeys'))
		// GALLERY
		->addFields(get_field_partial('partials.subfields.gallery1'))
->endRepeater()
	// TESTIMONIALS
->addRepeater('testimonials', [
	'label' => 'Testimonials',
])
->addText('paragraph', [
	'label' => 'Review',
])
->addText('name', [
	'label' => 'Name',
])
->addText('position', [
	'label' => 'Role, Company',
])
->endRepeater()

	// SECTION HERO 3
	->addImage('image3', [
		'label' => 'Image 3',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => [],
		'wrapper' => [
				'width' => '',
				'class' => '',
				'id' => '',
		],
		'return_format' => 'array',
])

	// RESULTS
	->addRepeater('numbers', [
		'label' => 'Numbers',
	])
	->addText('number', [
		'label' => 'Number',
	])
	->addText('result', [
		'label' => 'Definition',
	])
	->endRepeater()
	// RESULTS PARAGRAPH
	->addText('briefly', [
		'label' => 'Briefly',
	])
	// CTA TEXT
	->addText('cta', [
		'label' => 'CTA',
	]);


return $works;