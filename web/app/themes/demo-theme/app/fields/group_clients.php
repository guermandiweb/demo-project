<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$args = [
	'position' => 'acf_after_title',
	'show_in_rest' => true,
];

$clients_list = new FieldsBuilder('gruppo_clients_list', $args);

$clients_list
    ->setLocation('post_type', '==', 'list');

$clients_list
	// CLIENTS LOGOS
	->addRepeater('clients', [
		'label' => 'Lavoriamo per',
	])
	->addImage('logo', [
	  'label' => 'Logo',
	  'return_format' => 'array',
	])
	->endRepeater();

return $clients_list;