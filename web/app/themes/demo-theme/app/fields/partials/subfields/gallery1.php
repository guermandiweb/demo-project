<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$gallery1 = new FieldsBuilder('gallery1');
$gallery1
->addRepeater('gallery', [
  'label' => 'Gallery',
])
->addImage('gallery1_image', [
  'label' => 'Gallery1 Image',
  'instructions' => '',
  'required' => 0,
  'conditional_logic' => [],
  'wrapper' => [
      'width' => '',
      'class' => '',
      'id' => '',
  ],
  'return_format' => 'array',
])
->addTrueFalse('not_full_width', [
'label' => 'Not Full Width?',
])
->addText('gallery1_image_description', [
'label' => 'Gallery1 Image Description',
])
->endRepeater();

return $gallery1;