<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$video = new FieldsBuilder('video');
$video
->addUrl('video_url', [
		'label' => 'Video URL',
		'instructions' => 'Copiare la URL dal video inserito nella media library.<br>Nel caso di embeds da YouTube/Vimeo etc.: copiare solo la URL dal codice <iframe> (es. "https://www.youtube.com/embed/2b-PNcwWFgM").<br>La URL dovrebbe contenere il termine "embed" perché sia valida.',
		'required' => 0,
		'conditional_logic' => [],
		'wrapper' => [
				'width' => '',
				'class' => '',
				'id' => '',
		],
		'default_value' => '',
		'placeholder' => '',
	])
	->addText('video_description', [
		'label' => 'Video Description',
  ]);

  return $video;