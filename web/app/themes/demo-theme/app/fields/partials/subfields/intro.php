<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$intro = new FieldsBuilder('intro');
$intro
->addRepeater('paragraphs', [
		'label' => 'Introduction',
	])
	->addText('introduction', [
		'label' => 'Paragraphs',
	])
	->endRepeater();

return $intro;