<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$image2 = new FieldsBuilder('image2');
$image2
->addImage('image2', [
		'label' => 'Image 2',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => [],
		'wrapper' => [
				'width' => '',
				'class' => '',
				'id' => '',
		],
		'return_format' => 'array',
])
->addTrueFalse('not_full_width_2', [
	'label' => 'Not Full Width?',
])
->addText('image2_description', [
	'label' => 'Image 2 Description',
]);

return $image2;