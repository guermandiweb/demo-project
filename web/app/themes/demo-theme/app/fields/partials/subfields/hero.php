<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$hero = new FieldsBuilder('hero');
$hero
    ->addText('motto', [
      'label' => 'Motto',
    ])
    ->addImage('hero_image', [
      'label' => 'Hero Image',
      'return_format' => 'array',
    ]);

return $hero;