<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$descriptionkeys = new FieldsBuilder('descriptionkeys');
$descriptionkeys
->addRepeater('keywords_field', [
			'label' => 'Keywords',
		])
		->addText('keywords_text', [
			'label' => 'Keywords',
		])
		->endRepeater();

    return $descriptionkeys;