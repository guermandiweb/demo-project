<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$what = new FieldsBuilder('what');
$what
->addRepeater('list', [
		'label' => 'Our Work',
	])
	->addText('item', [
		'label' => 'What we did',
	])
	->endRepeater();

return $what;