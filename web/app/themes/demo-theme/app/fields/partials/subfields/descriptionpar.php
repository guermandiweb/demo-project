<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$descriptionpar = new FieldsBuilder('descriptionpar');
$descriptionpar
->addRepeater('descr_field', [
			'label' => 'Description Paragraph',
		])
		->addText('descr_paragraph', [
			'label' => 'Description Paragraph',
		])
		->endRepeater();

    return $descriptionpar;