<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$image1 = new FieldsBuilder('image1');
$image1
->addImage('image1', [
		'label' => 'Image 1',
		'instructions' => '',
		'required' => 0,
		'conditional_logic' => [],
		'wrapper' => [
				'width' => '',
				'class' => '',
				'id' => '',
		],
		'return_format' => 'array',
])
->addTrueFalse('not_full_width_1', [
	'label' => 'Not Full Width?',
])
->addText('image1_description', [
	'label' => 'Image 1 Description',
]);

return $image1;