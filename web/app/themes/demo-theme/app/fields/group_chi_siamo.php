<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$args = [
	'position' => 'acf_after_title',
	'show_in_rest' => true,
];

$chi_siamo = new FieldsBuilder('chi_siamo', $args);

$chi_siamo
    ->setLocation('page_template', '==', 'views/page-chi-siamo.blade.php');
  
$chi_siamo
  // DESCRIPTIONS
	->addRepeater('chi_siamo', [
		'label' => 'Descrizioni',
		'min' => 1,
		'max' => 3,
	])
	->addText('line', [
		'label' => 'Frase',
	])
	->endRepeater()
	// CTA TEXT
	->addText('cta', [
		'label' => 'CTA',
	])
	// WORKFLOW SECTIONS
	->addRepeater('workflow', [
		'label' => 'Il nostro workflow',
	])
	->addImage('icon', [
	  'label' => 'Icon',
	  'return_format' => 'array',
	  'mime_types' => 'svg',
	])
	->addText('title', [
		'label' => 'Keyword',
	])
	->addText('descr', [
		'label' => 'Descrizione',
	])
	->endRepeater();

return $chi_siamo;