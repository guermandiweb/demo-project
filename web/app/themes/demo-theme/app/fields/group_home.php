<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$args = [
	'position' => 'acf_after_title',
	'show_in_rest' => true,
];

$home = new FieldsBuilder('home', $args);

$home
    ->setLocation('page_template', '==', 'views/front-page.blade.php');
  
$home
	// MOTTO
	->addText('agency_motto', [
		'label' => 'Agency Motto',
	])
	// CTA TEXT
	->addText('cta', [
		'label' => 'CTA',
	])
	// VIDEO HERO
	->addUrl('video_hero_url', [
		'label' => 'Video URL',
		'instructions' => 'Copiare la URL dal video inserito nella media library',
		'required' => 0,
		'conditional_logic' => [],
		'wrapper' => [
				'width' => '',
				'class' => '',
				'id' => '',
		],
		'default_value' => '',
		'placeholder' => '',
	])
	// HERO IMAGES
// 	->addRepeater('hero_images', [
// 		'label' => 'Hero Images'
// 	])
// 	->addImage('hero_image', [
// 		'label' => 'Hero Image',
// 		'return_format' => 'array',
// ])
// ->endRepeater()
	// SERVICES
	->addWysiwyg('service1', [
	  'label' => 'Service1',
		'instructions' => 'Please, try to not edit this field',
		'default_value' => '<h1 class=""><span class="pink">strategia digitale</span> design <span class="pink">contenuti advertising eventi</span></h1>',
		'tabs' => 'text',
		'toolbar' => 'basic',
	])
	->addWysiwyg('service2', [
	  'label' => 'Service2',
		'instructions' => 'Please, try to not edit this field',
		'default_value' => '<h1 class=""><span class="pink">strategia digitale design</span> contenuti <span class="pink">advertising eventi</span></h1>',
		'tabs' => 'text',
		'toolbar' => 'basic',
	])
	// ->addWysiwyg('service3', [
	//   'label' => 'Service3',
	// 	'instructions' => 'Please, try to not edit this field',
	// 	'default_value' => '<h1 class=""><span class="pink">design strategia</span> digitale <span class="pink">advertising eventi contenuti</span></h1>',
	// 	'tabs' => 'text',
	// 	'toolbar' => 'basic',
	// ])
	// ->addWysiwyg('service4', [
	//   'label' => 'Service4',
	// 	'instructions' => 'Please, try to not edit this field',
	// 	'default_value' => '<h1 class=""><span class="pink">strategia digitale design contenuti</span> advertising <span class="pink">eventi</span></h1>',
	// 	'tabs' => 'text',
	// 	'toolbar' => 'basic',
	// ])
	->addWysiwyg('service5', [
	  'label' => 'Service5',
		'instructions' => 'Please, try to not edit this field',
		'default_value' => '<h1 class=""><span class="pink">digitale</span> strategia <span class="pink">design contenuti advertising eventi</span></h1>',
		'tabs' => 'text',
		'toolbar' => 'basic',
	])
	->addWysiwyg('service6', [
	  'label' => 'Service6',
		'instructions' => 'Please, try to not edit this field',
		'default_value' => '<h1 class=""><span class="pink">strategia digitale design contenuti</span> eventi <span class="pink">advertising</span></h1>',
		'tabs' => 'text',
		'toolbar' => 'basic',
	]);

return $home;