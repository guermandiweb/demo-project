set :stage, :staging
# set :webdir, 'staging.guermandi.it/guermandi2022.staging.guermandi.it'
set :webdir, '/var/www/guermandigroup/data/www/guermandi2022.staging.guermandi.it'

#usato per le sostituzioni WPCLI
set :wpcli_remote_url, 'guermandi2022.staging.guermandi.it'
set :wpcli_local_url, 'guermandi2022.test'
set :wpcli_args, '--skip-columns=guid'

# Simple Role Syntax
# ==================
#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

# Extended Server Syntax
# ======================
# server 'guermandi2022.staging.guermandi.it', user: 'staging', roles: %w{web app db}, port: 456
server 'vps0311.00gate.com', user: 'guermandigroup', roles: %w{web app db}
# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
#  set :ssh_options, {
#    keys: %w(~/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }

fetch(:default_env).merge!(wp_env: :staging)
